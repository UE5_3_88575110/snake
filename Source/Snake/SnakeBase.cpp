// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(MovementSpeed);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SnakeElements.Num() > 0)
	{
		Move();

		CanChangeDirection = true;
	}
}

void ASnakeBase::FillSnakeElements()
{
	StartLength = StartLength > 0 ? StartLength : 1;
	
	for (int i = 0; i < StartLength; i++)
	{
		AddSnakeElement();
	}
}

void ASnakeBase::AddSnakeElement()
{
	FVector NewLocation = GetActorLocation();
	
	if (SnakeElements.Num() > 0)
	{
		NewLocation = SnakeElements.Last()->GetActorLocation();

		switch (CurrentMovementDirection)
		{
			case EMovementDirection::UP:
				NewLocation.X -= ElementSize;
				break;
			case EMovementDirection::DOWN:
				NewLocation.X += ElementSize;
				break;
			case EMovementDirection::LEFT:
				NewLocation.X += ElementSize;
				break;
			case EMovementDirection::RIGHT:
				NewLocation.X -= ElementSize;
				break;
		}
	}
	
	const FTransform NewTransform(NewLocation);

	const auto SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	SnakeElement->SnakeOwner = this;

	const int32 ElementIndex = SnakeElements.Add(SnakeElement);

	if (ElementIndex == 0)
	{
		SnakeElement->SetFirstElementType();
	}

	OnSizeChangedDelegate.Broadcast(SnakeElements.Num() - StartLength);
}

void ASnakeBase::Move()
{
	FVector MovementVector;
	
	switch (CurrentMovementDirection)
	{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y -= ElementSize;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y += ElementSize;
			break;
	}

	// Сперва двигаем голову
	auto PrevElementLocation = SnakeElements[0]->GetActorLocation();
	SnakeElements[0]->AddActorWorldOffset(MovementVector, true);

	for (int i = 1; i < SnakeElements.Num(); i++)
	{
		const auto TmpElementLocation = SnakeElements[i]->GetActorLocation();
		
		SnakeElements[i]->SetActorLocation(PrevElementLocation, true);

		PrevElementLocation = TmpElementLocation;
	}
}

EMovementDirection ASnakeBase::GetCurrentMovementDirection()
{
	return CurrentMovementDirection;
}

void ASnakeBase::SetMovementDirection(EMovementDirection MovementDirection)
{
	if (CanChangeDirection)
	{
		CurrentMovementDirection = MovementDirection;

		CanChangeDirection = false;
	}
}


void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlapElement, AActor* OtherActor)
{
	if (IsValid(OverlapElement) && OverlapElement->IsFirstElement)
	{
		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);

		if (InteractableInterface)
		{
			InteractableInterface->Interact(this);
		}
	}
}

TArray<ASnakeElementBase*> ASnakeBase::GetElements()
{
	return this->SnakeElements;
}


