// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

UCLASS()
class SNAKE_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();
	
	UPROPERTY(EditDefaultsOnly);
	FVector StartSnakeLocation {700, 1200, 50};
	
	UPROPERTY(BlueprintReadWrite)
	class UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	class ASnakeBase* SnakeActor;

	UPROPERTY(BlueprintReadWrite)
	class AFoodGenerator* FoodGenerator;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFoodGenerator> FoodGeneratorClass;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	void CreateFoodGeneratorActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	UFUNCTION(BlueprintCallable)
	void StartGame();
};
