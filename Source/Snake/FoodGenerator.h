// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodGenerator.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFreeCellsEndedSignature);

UCLASS()
class SNAKE_API AFoodGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodGenerator();

	UPROPERTY(BlueprintAssignable)
	FOnFreeCellsEndedSignature OnFreeCellsEndedDelegate;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly);
	int32 RowCellsCount = 0;

	UPROPERTY(EditDefaultsOnly);
	int32 ColumnCellsCount = 0;

	UPROPERTY(EditDefaultsOnly);
	float CellSize = 100.f;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class AFoodBase> FoodActorClass;

	UPROPERTY()
	class ASnakeBase* Snake = nullptr;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void StartGeneration(ASnakeBase* SnakeActor);

	UFUNCTION()
	void OnFoodDestroyHandler(AActor* DestroyedActor);

protected:

	UFUNCTION()
	void GenerateFoodItem();

	UFUNCTION()
	TArray<FVector> GetFreeCells();
};
