// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"

#include "FoodGenerator.h"
#include "SnakeBase.h"
#include "Camera/CameraComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	SetRootComponent(PawnCamera);
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));

	CreateSnakeActor();
	CreateFoodGeneratorActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(StartSnakeLocation));
}

void APlayerPawnBase::CreateFoodGeneratorActor()
{
	FoodGenerator = GetWorld()->SpawnActor<AFoodGenerator>(FoodGeneratorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->GetCurrentMovementDirection() != EMovementDirection::DOWN)
		{
			SnakeActor->SetMovementDirection(EMovementDirection::UP);
		}
		else if (value < 0 && SnakeActor->GetCurrentMovementDirection() != EMovementDirection::UP)
		{
			SnakeActor->SetMovementDirection(EMovementDirection::DOWN);
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->GetCurrentMovementDirection() != EMovementDirection::LEFT)
		{
			SnakeActor->SetMovementDirection(EMovementDirection::RIGHT);
		}
		else if (value < 0 && SnakeActor->GetCurrentMovementDirection() != EMovementDirection::RIGHT)
		{
			SnakeActor->SetMovementDirection(EMovementDirection::LEFT);
		}
	}
}

void APlayerPawnBase::StartGame()
{
	SnakeActor->FillSnakeElements();
	FoodGenerator->StartGeneration(SnakeActor);
}

