// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodGenerator.h"

#include "FoodBase.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
AFoodGenerator::AFoodGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodGenerator::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFoodGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFoodGenerator::StartGeneration(ASnakeBase* SnakeActor)
{
	Snake = SnakeActor;

	GenerateFoodItem();
}

void AFoodGenerator::OnFoodDestroyHandler(AActor* DestroyedActor)
{
	GenerateFoodItem();
}

void AFoodGenerator::GenerateFoodItem()
{
	auto FreeCells = GetFreeCells();

	if (FreeCells.Num() > 0)
	{
		const int32 CellIndex = FMath::RandRange(0, FreeCells.Num() - 1);

		const FTransform SpawnLocation(FreeCells[CellIndex]);

		const auto FoodItem = GetWorld()->SpawnActor<AFoodBase>(FoodActorClass, SpawnLocation);
		
		FoodItem->OnDestroyed.AddDynamic(this, &AFoodGenerator::OnFoodDestroyHandler);
	}
	else
	{
		OnFreeCellsEndedDelegate.Broadcast();
	}	
}

TArray<FVector> AFoodGenerator::GetFreeCells()
{
	TArray<FVector> FreeCells;

	// Fill
	for (int Row = 0; Row < RowCellsCount; Row++)
	{
		for (int Col = 0; Col < ColumnCellsCount; Col++)
		{
			FreeCells.Add(FVector(Col * CellSize, Row * CellSize, 50));
		}
	}

	if (Snake->GetElements().Num() > 0)
	{
		for (const auto SnakeElement : Snake->GetElements())
		{
			FVector Location = SnakeElement->GetActorLocation();

			const int32 FindIndex = FreeCells.Find(Location);
		
			if (FindIndex != INDEX_NONE)
			{
				FreeCells.RemoveAt(FindIndex);
			}
		}
	}

	return FreeCells;
}

