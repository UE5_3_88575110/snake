// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSizeChangedSignature, int32, Size);

UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly);
	TSubclassOf<class ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly);
	float MovementSpeed = 0.5f;
	
	UPROPERTY(EditDefaultsOnly);
	float ElementSize = 100.f;;

	UPROPERTY(EditDefaultsOnly);
	int32 StartLength = 3;

	UPROPERTY(BlueprintAssignable)
	FOnSizeChangedSignature OnSizeChangedDelegate;
	
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	EMovementDirection CurrentMovementDirection = EMovementDirection::DOWN;;

	UPROPERTY()
	bool CanChangeDirection = false;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void FillSnakeElements();

	UFUNCTION(BlueprintCallable)
	void AddSnakeElement();

	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	EMovementDirection GetCurrentMovementDirection();
	
	UFUNCTION()
	void SetMovementDirection(EMovementDirection MovementDirection);

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlapElement, AActor* OtherActor);

	UFUNCTION()
	TArray<ASnakeElementBase*> GetElements();
};
